package com.ucbcba.post.Services;

import com.ucbcba.post.Entities.User;

public interface UserService {

    Iterable<User> ListAllUsers();

    User getUserById(Integer id);

    User saveUser(User user);

    void deleteUser(Integer id);
}

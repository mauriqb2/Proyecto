package com.ucbcba.post.Controllers;

import com.ucbcba.post.Entities.Category;
import com.ucbcba.post.Entities.Post;
import com.ucbcba.post.Entities.Comment;
import com.ucbcba.post.Services.CategoryService;
import com.ucbcba.post.Services.CommentService;
import com.ucbcba.post.Services.PostService;
import com.ucbcba.post.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@Controller
public class PostController {

    private PostService postService;
    private CommentService commentService;
    private CategoryService categoryService;
    private UserService userService;

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @Autowired
    public void setUserService(UserService userService){
        this.userService=userService;
    }
    @Autowired
    public void setCommentService(CommentService commentService){
        this.commentService=commentService;
    }

    @RequestMapping("/")
    public String root(Model model){
        model.addAttribute("posts",postService.listAllPosts());
        return "root";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("posts", postService.listAllPosts());
        return "posts";
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    public String showPost(@PathVariable Integer id, Model model) {
        Post post = postService.getPostById(id);
        model.addAttribute("post",post);
        model.addAttribute("comment",new Comment(post));
        return "post";
    }

    @RequestMapping(value = "/post/like/{id}", method = RequestMethod.GET)
    public String addLike(@PathVariable Integer id, Model model) {
        postService.getPostById(id).setLikes(postService.getPostById(id).getLikes()+1);
        postService.savePost(postService.getPostById(id));
        return "redirect:/post/{id}";
    }

    @RequestMapping(value = "/post/dislike/{id}", method = RequestMethod.GET)
    public String addDislike(@PathVariable Integer id, Model model) {
        Post post=postService.getPostById(id);
        if(post.getLikes()==0){
            return "redirect:/post/{id}";
        }
        post.setLikes(post.getLikes()-1);
        postService.savePost(post);
        return "redirect:/post/{id}";
    }

    @RequestMapping(value = "/post/delete/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable Integer id, Model model) {
        for (Comment comment: postService.getPostById(id).getComments())
            commentService.deleteComment(comment.getId());
        postService.deletePost(id);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/post/new", method = RequestMethod.GET)
    public String newPost(Model model) {
        model.addAttribute("post",new Post());
        model.addAttribute("categories",categoryService.ListAllCategories());
        model.addAttribute("users",userService.ListAllUsers());
        return "postForm";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String save(@Valid Post post, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("categories",categoryService.ListAllCategories());
            model.addAttribute("users",userService.ListAllUsers());
            return "postForm";
        }
        postService.savePost(post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/post/editar/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable Integer id, Model model) {
        model.addAttribute("post",postService.getPostById(id));
        model.addAttribute("categories",categoryService.ListAllCategories());
        model.addAttribute("users",userService.ListAllUsers());
        return "postForm";
    }
}

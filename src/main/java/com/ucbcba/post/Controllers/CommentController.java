package com.ucbcba.post.Controllers;

import com.ucbcba.post.Entities.Comment;
import com.ucbcba.post.Entities.Post;
import com.ucbcba.post.Services.CommentService;
import com.ucbcba.post.Services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Controller
public class CommentController {

    private CommentService commentService;
    private PostService postService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setPostService(PostService postService){
        this.postService=postService;
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(@Valid Comment comment, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            Post post = postService.getPostById(comment.getPost().getId());
            model.addAttribute("post",post);
            return "post";
        }
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }

    @RequestMapping(value = "/comment/like/{id}", method = RequestMethod.GET)
    public String addLike(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        comment.setLikes(comment.getLikes()+1);
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }

    @RequestMapping(value = "/comment/dislike/{id}", method = RequestMethod.GET)
    public String addDislike(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        if(comment.getLikes()==0){
            return "redirect:/post/"+comment.getPost().getId();
        }
        comment.setLikes(comment.getLikes()-1);
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }
}

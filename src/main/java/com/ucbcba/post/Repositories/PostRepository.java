package com.ucbcba.post.Repositories;

import com.ucbcba.post.Entities.Post;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends CrudRepository<Post,Integer> {
}
